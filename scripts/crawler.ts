import fs from 'fs';
import path from 'path';
import consola from 'consola';
import fetchCoolPC from './crawler/coolpc.fetch';
import parseCoolPC from './crawler/coolpc.parse';

const DIST_DIR = path.resolve(__dirname, '../public/data');

const writeJsonFile = (filePath: string, data: any) => {
  if (!fs.existsSync(DIST_DIR)) fs.mkdirSync(DIST_DIR);

  fs.writeFileSync(filePath, JSON.stringify(data));
};

const getJsonFilePath = (name: string): string => {
  return path.join(DIST_DIR, `${name}.json`);
};

const runCoolPcProcess = async () => {
  consola.log('啟動爬蟲 -> 原價屋');

  const htmlText = await fetchCoolPC();
  const data = parseCoolPC(htmlText);
  const filePath = getJsonFilePath('COOLPC');

  consola.log(`寫入資料 -> ${filePath}`);

  writeJsonFile(filePath, data);
};

const cli = async () => {
  await runCoolPcProcess();
};

if (require.main === module) cli();
