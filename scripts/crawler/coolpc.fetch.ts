import axios, { AxiosResponse } from 'axios';
import iconv from 'iconv-lite';

const REGEX_HEADER_CHARSET = /charset=(.+)/;

function _getResponseCharset(response: AxiosResponse): string {
  const ctype: string = response.headers['content-type'];
  const matched = ctype.match(REGEX_HEADER_CHARSET);
  if (!matched || !matched.length || !matched[1]) return 'utf8';

  return matched[1].toLocaleLowerCase();
}

export default async () => {
  const response = await axios.request<Buffer>({
    method: 'GET',
    url: 'http://www.coolpc.com.tw/evaluate.php',
    responseType: 'arraybuffer',
  });

  const charset = _getResponseCharset(response);
  const html = iconv.decode(response.data, charset);

  return html;
};
