import cheerio from 'cheerio';

export interface ICoolPCData {
  updateAt: number;

  products: {
    name: string;
    category: string;
    price: number;
  }[];
}

const REGEX_PRODUCT_PRICE = /,\s.*↘?\$(\d+)/;
const REGEX_PRODUCT_PHOTO = /\s?★/;
const REGEX_PRODUCT_OPENBOX = /\s?◆/;

function _getUpdateAt($: cheerio.Root): ICoolPCData['updateAt'] {
  const $updateAt = $('#Mdy');
  const _updateAtTxt = $updateAt
    .contents()
    .first()
    .text();
  const _updateAt = new Date(_updateAtTxt);

  return _updateAt.getTime();
}

function _extractProductPrice(text: string): number | undefined {
  const matched = text.match(REGEX_PRODUCT_PRICE);
  if (!matched || !matched.length) return;

  return new Number(matched[1]).valueOf();
}

function _extractProductName(text: string): string {
  return text
    .replace(REGEX_PRODUCT_PRICE, '')
    .replace(REGEX_PRODUCT_PHOTO, '')
    .replace(REGEX_PRODUCT_OPENBOX, '')
    .trim();
}

function _getProductList($: cheerio.Root): ICoolPCData['products'] {
  const list: ICoolPCData['products'] = [];
  $('#tbdy > tr').each((ti, tr) => {
    const $tr = $(tr);
    const $category = $tr.find('td.t').first();
    const $select = $tr.find('td > select').first();
    const $options = $select.find('option:not(:disabled):not([value="0"])');

    $options.each((oi, opt) => {
      const $option = $(opt);
      const txt = $option.text();
      const category = $category.text();
      const price = _extractProductPrice(txt);
      const name = _extractProductName(txt);

      list.push({
        category,
        price,
        name,
      });
    });
  });

  return list;
}

export default (htmlText: string): ICoolPCData => {
  const $ = cheerio.load(htmlText);

  return {
    updateAt: _getUpdateAt($),
    products: _getProductList($),
  };
};
