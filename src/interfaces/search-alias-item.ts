export interface ISearchAliasItem {
  name: string;
  value: string;
}
