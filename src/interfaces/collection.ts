import type { ICollectionProduct } from './collection-project';

export interface ICollection {
  name: string;
  updateAt: number;
  products: ICollectionProduct[];
}
