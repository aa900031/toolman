export interface ICollectionProduct {
  category: string;
  name: string;
  price: number;
}
