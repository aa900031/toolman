import { createRouter, createWebHashHistory } from 'vue-router';

export const ROUTE_NAME_COLLECTION_DETAIL = 'collection-detail';

export const history = createWebHashHistory();

export const router = createRouter({
  history: history,
  routes: [
    {
      name: ROUTE_NAME_COLLECTION_DETAIL,
      path: '/',
      component: () => import('./pages/Collection.vue'),
    },
  ],
});
