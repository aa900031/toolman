import hotkyes from 'hotkeys-js';
import { onMounted, onUnmounted } from 'vue';
import type { KeyHandler } from 'hotkeys-js';

type AvailableTags = 'INPUT' | 'TEXTAREA' | 'SELECT';

const tagFilter = ({ target, srcElement }: KeyboardEvent, enableOnTags?: AvailableTags[]) => {
  // @ts-ignore
  const targetTagName = (target && target.tagName) || (srcElement && srcElement.tagName);

  return Boolean(
    targetTagName && enableOnTags && enableOnTags.includes(targetTagName as AvailableTags),
  );
};

const isKeyboardEventTriggeredByInput = (ev: KeyboardEvent) => {
  return tagFilter(ev, ['INPUT', 'TEXTAREA', 'SELECT']);
};

export const useHotkey = (key: string, callback: KeyHandler) => {
  const handler: KeyHandler = (event, hotkeyEvent) => {
    if (isKeyboardEventTriggeredByInput(event)) {
      return true;
    }
    return callback(event, hotkeyEvent);
  };

  const bind = () => {
    hotkyes(key, handler);
  };

  const clear = () => {
    hotkyes.unbind(key, handler);
  };

  onMounted(() => {
    bind();
  });

  onUnmounted(() => {
    clear();
  });

  return {
    clear,
  };
};
