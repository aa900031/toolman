import axios from 'axios';
import { ref } from 'vue';
import type { ICollection } from '../interfaces/collection';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const useCollection = (name: string) => {
  const isFetching = ref<boolean>(false);
  const data = ref<ICollection | null>(null);
  const fetch = async () => {
    try {
      isFetching.value = true;
      const response = await axios.get<ICollection>(`/data/${name}.json`);
      data.value = response.data;
      // eslint-disable-next-line no-useless-catch
    } catch (error) {
      throw error;
    } finally {
      isFetching.value = false;
    }
  };

  return {
    fetch,
    data,
    isFetching,
  };
};
