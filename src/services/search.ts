import _ from 'lodash';
import { type Ref, ref, unref, computed } from 'vue';
import type { ICollection } from '../interfaces/collection';
import type { ICollectionProduct } from '../interfaces/collection-project';
import type { ISearchAliasItem } from '../interfaces/search-alias-item';
import { useDebounce } from '@vueuse/core';

interface TextPattern {
  regex: RegExp;
  count: number;
}

interface PricePattern {
  min: number | null;
  max: number | null;
}

interface Rule {
  text: TextPattern;
  price: PricePattern[] | null;
}

const aliases: ISearchAliasItem[] = [
  {
    name: 'ASUS',
    value: '華碩',
  },
  {
    name: '宏碁',
    value: 'Acer',
  },
  {
    name: '宏達電',
    value: 'HTC',
  },
  {
    name: 'GIGABYTE',
    value: '技嘉',
  },
  {
    name: 'Kingston',
    value: '金士頓',
  },
  {
    name: '群暉',
    value: 'Synology',
  },
  {
    name: '惠普',
    value: 'HP',
  },
  {
    name: '聯想',
    value: 'Lenovo',
  },
  {
    name: 'ADATA',
    value: '威剛',
  },
  {
    name: 'MSI',
    value: '微星',
  },
  {
    name: 'ASRock',
    value: '華擎',
  },
  {
    name: '東芝',
    value: 'Toshiba',
  },
  {
    name: '希捷',
    value: 'Seagate',
  },
  {
    name: 'Cooler Master',
    value: '酷碼',
  },
];

const reSpace = /[\s]/;

const rePriceSign = /^\$/;

const rePriceChunk = /^\$(\d+)(?:([+-])?|~(\d+))?$/;

const rePriceRange = /^(\d+)(?:([+-])?|~(\d+))?$/;

const parseChunk = (
  query: string,
): {
  text: string[];
  price: string[];
} =>
  query
    .trim()
    .split(reSpace)
    .map(s => s.trim())
    .reduce(
      (obj, chunk) => {
        if (rePriceChunk.test(chunk)) {
          obj.price.push(chunk.replace(rePriceSign, ''));
        } else {
          obj.text.push(chunk);
        }
        return obj;
      },
      { text: [], price: [] } as { text: string[]; price: string[] },
    );

const createTextPattern = (chunk: string[]): TextPattern => {
  const regex = chunk
    .map(s => {
      const _aliases = aliases
        .filter(alias => new RegExp(`${alias.name}`, 'i').test(s))
        .map(alias => alias.value);
      const _pattern = [s, ..._aliases].map(str => _.escapeRegExp(str)).join('|');
      return `(${_pattern})`;
    })
    .join('|');

  return {
    regex: new RegExp(regex, 'ig'),
    count: chunk.length,
  };
};

const parsePriceRange = (query: string): PricePattern | void => {
  const matched = rePriceRange.exec(query);
  if (!Array.isArray(matched)) return;
  const [, price1, sign, price2] = matched;

  const numPrice1 = +price1;
  const numPrice2 = +price2;
  if (_.isNumber(numPrice1) && !isNaN(numPrice1)) {
    if (_.isNumber(numPrice2) && !isNaN(numPrice2)) {
      return {
        min: Math.min(numPrice1, numPrice2),
        max: Math.max(numPrice1, numPrice2),
      };
    } else if (sign === '-') {
      return {
        min: null,
        max: numPrice1,
      };
    } else {
      return {
        min: numPrice1,
        max: null,
      };
    }
  } else {
    return;
  }
};

const createPricePattern = (chunk: string[]): PricePattern[] | null => {
  const val = chunk.reduce((all, txt) => {
    const range = parsePriceRange(txt);
    if (range) all.push(range);
    return all;
  }, [] as PricePattern[]);

  return Array.isArray(val) && val.length > 0 ? val : null;
};

const createCheckMatch = (regex: RegExp) => {
  const doMatch = (val: string[]): RegExpMatchArray[] => val.map(item => item.match(regex) || []);

  return _.flow(
    // 為每個值套用正則表示法測試
    doMatch,
    // 將 RegExpMatch[][] 壓平 -> RegExpMatch[]
    _.flattenDeep,
    // 將 RegExpMatch 轉成純陣列
    _.toArray,
    // 讓陣列內的值唯一化
    _.uniq,
  );
};

export const useSearch = (collection: Ref<ICollection | null>, defaultQuery = '') => {
  const query = ref<string>(defaultQuery);
  const queryDebounced = useDebounce(query, 300);
  const rules = computed<Rule | void>(() => {
    const _query = unref(queryDebounced);
    if (!_query) return;

    const { text: textChunk, price: priceChunk } = parseChunk(_query);
    return {
      text: createTextPattern(textChunk),
      price: createPricePattern(priceChunk),
    };
  });
  const results = computed<ICollectionProduct[]>(() => {
    const _collection = unref(collection);
    if (!_collection || !_collection.products || !_collection.products.length) {
      return [];
    }
    const _rules = unref(rules);
    if (!_rules) {
      return _collection.products;
    }

    const { text, price } = _rules;
    const checkMatch = createCheckMatch(text.regex);

    return _collection.products.filter(project => {
      const textMatch =
        checkMatch([project.name || '', project.category || '']).length >= text.count;

      const priceMatch = price
        ? price.some(range => {
            const hasMax = _.isNumber(range.max);
            const hasMin = _.isNumber(range.min);
            if (hasMax && hasMin) {
              return project.price >= range.min! && project.price <= range.max!;
            } else if (hasMin) {
              return project.price >= range.min!;
            } else if (hasMax) {
              return project.price <= range.max!;
            }
            return false;
          })
        : true;

      return textMatch && priceMatch;
    });
  });
  const count = computed(() => unref(results).length);

  return {
    query,
    results,
    count,
  };
};
